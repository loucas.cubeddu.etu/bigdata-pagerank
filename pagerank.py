import numpy as np
import numpy.linalg as nla

def pagerank(P :np.array, beta: np.float64 = .85, tolerance = 10**-9) -> np.array:
    """
    Trouve le pagerank de la matrice P.
    On effectue la méthode de power iterations pour trouver le vecteur
    propre dominant de R = beta*P + (beta-1)*Q qui est la mesure stationnaire 
    de beta* P.T + (1-beta)*Q càd la solution de pi = pi.dot(beta* P.T + (1-beta)*Q)

    Données:
        P : Transposée de la matrice d'adjacence avec colonnes normalisées
        beta 
        beta : proportion de la matrice P par rapport a la matrice de téléportation Q
        tolerance: on considère que le pagerank a convergé quand la distance entre deux
            itérations est inférieure à la tolérance
    Résultat:
        Un vecteur de taille n (avec P n x n) avec le pagerank des pages
    """
    n = P.shape[0]
    q1 = np.full(n, 1/n, dtype=np.float64)
    hasConverged = False

    while(not hasConverged):
        q0 = q1
        q1 = beta * P.dot(q0)
        q1 += np.full(n,((1-beta)/n)*np.sum(q0), dtype=np.float64)  

        q1 /= np.sum(q1)

        if(np.linalg.norm(q0-q1,2) < tolerance):
            hasConverged = True

    return q1


def networkx_pagerank(P, beta = .85, plot = False):
    """
    Pagerank of P using the networkx library
    """
    import networkx as nx
    nxgraph = nx.DiGraph(nx.from_scipy_sparse_array(P.T))

    if plot:
        pos=nx.spring_layout(nxgraph) # pos = nx.nx_agraph.graphviz_layout(G)
        nx.draw_networkx(nxgraph, pos)
        labels = nx.get_edge_attributes(nxgraph, 'weight')
        nx.draw_networkx_edge_labels(nxgraph ,pos, edge_labels=labels)

    pr = nx.pagerank(nxgraph, beta)
    return np.array(list(pr.values()), dtype=np.float64)