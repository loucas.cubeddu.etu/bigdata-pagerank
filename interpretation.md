# Question 03

Dans un premier lieu, on a implémenté l'algorithme de pageRank. Ensuite on a récupéré les données des navigations terminées. 
Lors de l'application de l'algorithme de pageRank sur les données on a eu l'odre d'importance des pages comme suit: 

```
United_States,0.02999984165170076
Europe,0.014264938003685935
United_Kingdom,0.012945952943351963
England,0.011416568144026636
Africa,0.009356302682468454
Earth,0.008104217032010478
World_War_II,0.007868375973718669
Germany,0.006039515258714436
North_America,0.005644612835300216
France,0.0055131812777977655
English_language,0.004592847491091488
Animal,0.004279690042093108
Atlantic_Ocean,0.004267838100119293
Periodic_table,0.004164920818632466
Human,0.004042313978916414
.
.
.
```

Pour la suite on a implémenté deux version de ranking des pages: 
La première sant prendre en compte le nombre des navigation entre deux pages ce qui a donnée un graphe générique où les poinds des arcs sont des 1. 
La deuxième prend en compte le nombre d'utilisateur qui ont fait le chemin entre une page A et une page B. Ce qui fait que les poinds des arcs maintenant sans des fréquence 
le navigation entre les pages. Les résultat des deux implémentation donnent différents résultat ce qui est normal vu que la matrice P sur lequel pageRank est appliqué n'est pas la même. 

Conclusion tirée: 
on conclue, que les résultats de pageRank sont influencé par la matrice de transition et l'hyper-paramètre damping factor comme on l'a vu dans 
les implémentation ci-dessus. 
 
