# TODO: Move most functions into separate files
# TODO: go object oriented

import sys
from getopt import GetoptError, getopt
import numpy as np
import numpy.linalg as nla
import matplotlib.pyplot as plt
import pandas as pd
from scipy import sparse
from pagerank import pagerank

def add_to_successors_dict(successors, line):
    """
    Adds the line that stores a player's wikispeedia navigation history
    to the successors dictionary by associating each page (key) to its set of successors (value)

    Data :
        successors: a dictionary that associates Key = Page -> Set of successor pages = Value
        line: the line of navigation history (this is a list of strings, each string retracing 
                the nav history, and `<` meaning the player went back one page)
    Results :
        successors: Modifies successors dict. Adds the info stored in the line.
    """
    i = 0
    while i < len(line) - 1:
        page = line[i]
        page_next = line[i+1]
        successors.setdefault(page, dict()) # Set successors[page] to the empty set only if the value doesn't already exist
        if page_next == '<':
            line.pop(i+1)
            line.pop(i)
            i -= 1
            continue

        successors.setdefault(page_next, dict()) # Since we now know the next page is not '<', we add it to the dict
        
        successors[page].setdefault(page_next, 0)  # Add the next page as a successor to the current page
        successors[page][page_next] += 1
        i += 1

def get_navigation_history_series(path):
    """
    Get a pandas Series with every element being a user's navigation history through wikispedia
    Data :
        path : the path to the file 
    Return :
        A pandas Series with every element being a user's navigation history through wikispedia (a string)
    """
    df = pd.read_csv(path, sep='\t', comment='#', header=None) # Tabulation Separated Values
    histories = df[3].str.split(";") # 3rd column is the web history of the player. We split every string around ";"
    return histories
        

def get_successors_data(path): 
    """
    Get the data in the form of a dictionnary with a point as the key and the set of its successors as the value
    Data :
        path : the path to the file 
    Return :
        A dictionnary with pages as keys and their successors as values
    """
    # Get the user's navigation histories in the form of a 
    # Pandas Series with each element being a list of strings representing
    # Pages the user has gone through
    histories = get_navigation_history_series(path)

    # For each wikispedia navigation history, we update the successors dictionary
    successors = dict()
    for line in histories:
        add_to_successors_dict(successors, line)

    return successors

def create_P_from_successors(successors: dict):
    """
    We create a scipy sparse matrix using csr_array from the successors dict.
    This matrix is the transpose of the markov transitions matrix
    such that its dominant eigenvector is the markov stationary measure.
    Data :
        successors: dictionnary that associate a page to the set of its successors
    Results : 
        P : The transpose of the markov transitions stochiastic matrix. 
        pages:  list of all pages in the same order as P
    """
    # We create a list of pages to ensure a consistent index -> page matching
    pages = set(successors.keys())
    pages.update(*successors.values())
    pages = list(pages)

    # Construct a lookup table, as to facilitate page -> index matching
    page_index = {page : index for index, page in enumerate(pages)}

    row = []
    col = []
    data = []

    # Constructing the csr matrix by collecting the row, col, data triplet for each arc
    for index, page in enumerate(pages):
        # If the page has no successors, we add an arc that leads to itself
        weighted_num_successors = np.sum(list(successors.get(page).values()))
        if weighted_num_successors == 0:
            row.append(index)
            col.append(index)
            data.append(1)
            continue
        
        # We add an arc to each successor of page
        for successor, weight in successors.get(page).items():
            successor_index = page_index.get(successor)
            row.append(index)
            col.append(successor_index)
            data.append(weight/weighted_num_successors) # Each row sum = 1 because P is stochiastic

    # We use a scipy csr_array to store the sparse matrix efficiently, and for fast matrix-vector multiplication
    markov_transitions_matrix = sparse.csr_array((data, (row, col)), dtype=np.float64)

    return markov_transitions_matrix.transpose(), pages


def save_data():
    print("==Loading Data, collecting successors==")
    successors = get_successors_data("wikispeedia_paths-and-graph/paths_finished.tsv")

    print("==Creating P sparse matrix==")
    P, pages = create_P_from_successors(successors)
        
    print("==Saving P and pages==")
    sparse.save_npz("scipy_sparse_P_matrix.npz", P)
    with open("pages.pkl", "wb") as f:
        import pickle
        pickle.dump(pages, f)

def load_data():
    print("==Loading P and pages==")
    try:
        P = sparse.load_npz("scipy_sparse_P_matrix.npz")
        with open("pages.pkl", "rb") as f:
            import pickle
            pages = pickle.load(f)
    except FileNotFoundError as e:
        print(f"{e.filename} does not exist!")
        print("Please run the app in SAVE mode before ranking")
        return

    return P, pages

def rank_data(P, pages, beta = .85, tol = 10**-9):
    pr = pagerank(P, beta, tol)
    pr_df = pd.DataFrame()
    pr_df["page"] = pages
    pr_df["rank"] = pr

    return pr_df

def help_menu():
    """
    Displays the help menu for the cli app
    """
    print(f"usage: {sys.argv[0]} [options]")
    print("Options and arguments:")
    print("\t-m modes or --mode modes: Choose app run mode. modes: [save | rank]. Default=\"save, rank\"")
    print("\t-b betas or --beta betas: Choose the beta or betas to use during pagerank. Default= 0.85")
    print("\t-t tols or --tol tols: Choose the tolerances for the convergeance of the rank. Default= 1e-9")

def get_params(opts, args):
    """
    Get pagerank parameters from the cli options and arguments
    Data:
        opts: list of cli options
        args: list of cli arguments
    Return:
        modes: list of AppState modes
        betas: list of beta parameters for pagerank
        tols: list of tolerances for rank convergence
    """
    # Default parameters
    modes = [AppState.SAVE, AppState.RANK]
    betas = [0.85]
    tols = [10**-9]
    
    ### TODO: Make this safe with try excepts
    for opt, arg in opts:
        if opt in ("-m", "--mode"):
            mode_strings = "".join(arg.upper().split()).split(",")
            modes = [AppState._member_map_.get(mode_string) for mode_string in mode_strings]
        elif opt in ("-b", "--beta"):
            beta_strings = "".join(arg.split()).split(",")
            betas = [float(beta_string) for beta_string in beta_strings]
        elif opt in ("-t", "--tol"):
            tol_strings = "".join(arg.split()).split(",")
            tols = [float(tol_string) for tol_string in tol_strings]

    return modes, betas, tols

from enum import Enum
class AppState(Enum):
    RANK = 0
    SAVE = -1

def main(argv):
    np.seterr(all='raise')

    try:
        opts, args = getopt(argv, "m:b:t:h", ["mode=", "beta=", "tol=", "help"])
    except GetoptError:
        help_menu()
        return

    # If user inputed help option:
    if any(help_str in (opt for opt, arg in opts) for help_str in ("-h", "--help")):
        help_menu()
        return # END SCRIPT

    modes, betas, tols = get_params(opts, args)

    if AppState.SAVE in modes:
        save_data()


    if AppState.RANK in modes:
        P, pages = load_data()
        
        if AppState.RANK in modes:
            pr_dfs = {(beta, tol) : rank_data(P, pages, beta, tol) for beta in betas for tol in tols}
            for (beta, tol), df in pr_dfs.items():
                df.sort_values(by=["rank"], ascending=False).to_csv("pagerank-b%.3f-t%.3E.csv" % (beta, tol))




if __name__ == "__main__":
    main(sys.argv[1:])